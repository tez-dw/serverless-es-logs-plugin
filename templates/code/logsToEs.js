// v1.1.2
var https = require('https');
var zlib = require('zlib');
var crypto = require('crypto');
var AWS = require('aws-sdk');

var endpoint = process.env.ES_ENDPOINT;
var assumerole = process.env.ES_ASSUMEROLE;
var indexPrefix = process.env.ES_INDEX_PREFIX;
var tags = undefined;
try {
    tags = JSON.parse(process.env.ES_TAGS);
} catch (_) {}

exports.handler = function(input, context) {
    // decode input from base64
    var zippedInput = new Buffer(input.awslogs.data, 'base64');

    // decompress the input
    zlib.gunzip(zippedInput, function(error, buffer) {
        if (error) { context.fail(error); return; }

        // parse the input from JSON
        var awslogsData = JSON.parse(buffer.toString('utf8'));

        // transform the input to Elasticsearch documents array
        var elasticsearchBulkArray = transform(awslogsData);

        // skip control messages
        if (!elasticsearchBulkArray) {
            console.log('Received a control message');
            context.succeed('Control message handled successfully');
            return;
        }

        var elasticsearchBulkPayload = esDocumentArrayToPayload(elasticsearchBulkArray);

        // post documents to the Amazon Elasticsearch Service
        post(elasticsearchBulkPayload, function(error, success, statusCode, failedItems) {
            console.log('Response: ' + JSON.stringify({ 
                "statusCode": statusCode 
            }));

            if (error) { 
                console.log('Error: ' + JSON.stringify(error, null, 2));

                if (failedItems && failedItems.length > 0) {
                    console.log("Failed Items: " +
                        JSON.stringify(failedItems, null, 2));

                    var failedLogs = findFailedLogs(failedItems, elasticsearchBulkArray);
                    failedLogs.forEach( failedLog => {
                        console.log("Failed log content: " + JSON.stringify(failedLog, null, 2));
                    });
                }

                context.fail(JSON.stringify(error));
            } else {
                console.log('Success: ' + JSON.stringify(success));
                context.succeed('Success');
            }
        });
    });
};

function findFailedLogs(failedItems, esBulkArray) {
    var failedLogs = [];

    failedItems.forEach(item => {
        var _id = item && item.index && item.index._id;
        if (_id) {
            var failedLog = esBulkArray.find(esLog => esLog && esLog.id === _id);
            if (failedLog) {
                failedLogs.push(failedLog);
            }
        }
    });

    return failedLogs;
}

function esDocumentArrayToPayload(esDocumentArray) {
    return esDocumentArray.map(
        item => {
            return [
                JSON.stringify(item.action),
                JSON.stringify(item.source)
            ].join('\n');
        }
    ).join('\n') + '\n';
}

function transform(payload) {
    if (payload.messageType === 'CONTROL_MESSAGE') {
        return null;
    }

    var bulkRequest = [];

    payload.logEvents.forEach(function(logEvent) {
        var timestamp = new Date(1 * logEvent.timestamp);

        // index name format: cwl-YYYY.MM.DD
        var indexName = [
            indexPrefix + '-' + timestamp.getUTCFullYear(),   // year
            ('0' + (timestamp.getUTCMonth() + 1)).slice(-2),  // month
            ('0' + timestamp.getUTCDate()).slice(-2)          // day
        ].join('.');

        var id = logEvent.id;

        var source = buildSource(logEvent.message, logEvent.extractedFields);
        source['@id'] = id;
        source['@timestamp'] = new Date(1 * logEvent.timestamp).toISOString();
        source['@message'] = logEvent.message;
        source['@owner'] = payload.owner;
        source['@log_group'] = payload.logGroup;
        source['@log_stream'] = payload.logStream;
        if (tags) {
            source['@tags'] = tags;
        }

        var action = { "index": {} };
        action.index._index = indexName;
        action.index._type = 'serverless-es-logs';
        action.index._id = id;
        
        bulkRequest.push({ id, action, source });
    });
    return bulkRequest;
}

function buildSource(message, extractedFields) {
    var jsonSubString = null;
    if (extractedFields) {
        var source = {};

        for (var key in extractedFields) {
            if (extractedFields.hasOwnProperty(key) && extractedFields[key]) {
                var value = extractedFields[key];

                if (isNumeric(value)) {
                    source[key] = 1 * value;
                    continue;
                }

                jsonSubString = extractJson(value);
                if (jsonSubString !== null) {
                    source['@' + key] = JSON.parse(jsonSubString);
                }

                source[key] = (key === 'apigw_request_id') ? value.slice(1, value.length - 1) : value;
            }
        }
        return source;
    }

    jsonSubString = extractJson(message);
    if (jsonSubString !== null) { 
        return JSON.parse(jsonSubString); 
    }

    return {};
}

function extractJson(message) {
    var jsonStart = message.indexOf('{');
    if (jsonStart < 0) return null;
    var jsonSubString = message.substring(jsonStart);
    return isValidJson(jsonSubString) ? jsonSubString : null;
}

function isValidJson(message) {
    try {
        JSON.parse(message);
    } catch (e) { return false; }
    return true;
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

async function post(body, callback) {
    var requestParams = await buildRequest(endpoint, assumerole, body);

    var request = https.request(requestParams, function(response) {
        var responseBody = '';
        response.on('data', function(chunk) {
            responseBody += chunk;
        });
        response.on('end', function() {
            var info = JSON.parse(responseBody);
            var failedItems;
            var success;
            
            if (response.statusCode >= 200 && response.statusCode < 299) {
                failedItems = info.items.filter(function(x) {
                    return x.index.status >= 300;
                });

                success = { 
                    "attemptedItems": info.items.length,
                    "successfulItems": info.items.length - failedItems.length,
                    "failedItems": failedItems.length
                };
            }

            var error = response.statusCode !== 200 || info.errors === true ? {
                "statusCode": response.statusCode,
                "responseBody": responseBody
            } : null;

            callback(error, success, response.statusCode, failedItems);
        });
    }).on('error', function(e) {
        console.error('Request to elastic failed');
        callback(e);
    });
    request.end(requestParams.body);
}

async function buildRequest(endpoint, assumerole, body) {
    let tempRole = null;
    let tempRoleJson = cache.get('tempRoleJson');
    if (!tempRoleJson) {
        console.log("Getting new credentials");
        const sts = new AWS.STS();
        tempRole = await sts.assumeRole({
            RoleArn: assumerole,
            RoleSessionName: 'tez-elastic-logger',
        }).promise();
        tempRoleJson = JSON.stringify(tempRole);
        cache.put('tempRoleJson', tempRoleJson, 3500 * 1000);
    } else {
        console.log("Using old credentials");
        tempRole = JSON.parse(tempRoleJson);
    }

    var endpointParts = endpoint.match(/^([^\.]+)\.?([^\.]*)\.?([^\.]*)\.amazonaws\.com$/);
    var region = endpointParts[2];
    var service = endpointParts[3];
    var datetime = (new Date()).toISOString().replace(/[:\-]|\.\d{3}/g, '');
    var date = datetime.substr(0, 8);
    var kDate = hmac('AWS4' + tempRole.Credentials.SecretAccessKey, date);
    var kRegion = hmac(kDate, region);
    var kService = hmac(kRegion, service);
    var kSigning = hmac(kService, 'aws4_request');
    
    var request = {
        host: endpoint,
        method: 'POST',
        path: '/_bulk',
        body: body,
        headers: { 
            'Content-Type': 'application/json',
            'Host': endpoint,
            'Content-Length': Buffer.byteLength(body),
            'X-Amz-Security-Token': tempRole.Credentials.SessionToken,
            'X-Amz-Date': datetime
        }
    };

    var canonicalHeaders = Object.keys(request.headers)
        .sort(function(a, b) { return a.toLowerCase() < b.toLowerCase() ? -1 : 1; })
        .map(function(k) { return k.toLowerCase() + ':' + request.headers[k]; })
        .join('\n');

    var signedHeaders = Object.keys(request.headers)
        .map(function(k) { return k.toLowerCase(); })
        .sort()
        .join(';');

    var canonicalString = [
        request.method,
        request.path, '',
        canonicalHeaders, '',
        signedHeaders,
        hash(request.body, 'hex'),
    ].join('\n');

    var credentialString = [ date, region, service, 'aws4_request' ].join('/');

    var stringToSign = [
        'AWS4-HMAC-SHA256',
        datetime,
        credentialString,
        hash(canonicalString, 'hex')
    ] .join('\n');

    request.headers.Authorization = [
        'AWS4-HMAC-SHA256 Credential=' + tempRole.Credentials.AccessKeyId + '/' + credentialString,
        'SignedHeaders=' + signedHeaders,
        'Signature=' + hmac(kSigning, stringToSign, 'hex')
    ].join(', ');

    return request;
}

function hmac(key, str, encoding) {
    return crypto.createHmac('sha256', key).update(str, 'utf8').digest(encoding);
}

function hash(str, encoding) {
    return crypto.createHash('sha256').update(str, 'utf8').digest(encoding);
}

/*
* Copied from https://github.com/ptarjan/node-cache/blob/f9d07a96d133b6fa1cd80864ad938ead91a8630a/index.js
* as adding dependencies aren't that straightforward
 */
function Cache () {
    var _cache = Object.create(null);
    var _hitCount = 0;
    var _missCount = 0;
    var _size = 0;
    var _debug = false;

    this.put = function(key, value, time, timeoutCallback) {
        if (_debug) {
            console.log('caching: %s = %j (@%s)', key, value, time);
        }

        if (typeof time !== 'undefined' && (typeof time !== 'number' || isNaN(time) || time <= 0)) {
            throw new Error('Cache timeout must be a positive number');
        } else if (typeof timeoutCallback !== 'undefined' && typeof timeoutCallback !== 'function') {
            throw new Error('Cache timeout callback must be a function');
        }

        var oldRecord = _cache[key];
        if (oldRecord) {
            clearTimeout(oldRecord.timeout);
        } else {
            _size++;
        }

        var record = {
            value: value,
            expire: time + Date.now()
        };

        if (!isNaN(record.expire)) {
            record.timeout = setTimeout(function() {
                _del(key);
                if (timeoutCallback) {
                    timeoutCallback(key, value);
                }
            }.bind(this), time);
        }

        _cache[key] = record;

        return value;
    };

    this.del = function(key) {
        var canDelete = true;

        var oldRecord = _cache[key];
        if (oldRecord) {
            clearTimeout(oldRecord.timeout);
            if (!isNaN(oldRecord.expire) && oldRecord.expire < Date.now()) {
                canDelete = false;
            }
        } else {
            canDelete = false;
        }

        if (canDelete) {
            _del(key);
        }

        return canDelete;
    };

    function _del(key){
        _size--;
        delete _cache[key];
    }

    this.clear = function() {
        for (var key in _cache) {
            clearTimeout(_cache[key].timeout);
        }
        _size = 0;
        _cache = Object.create(null);
        if (_debug) {
            _hitCount = 0;
            _missCount = 0;
        }
    };

    this.get = function(key) {
        var data = _cache[key];
        if (typeof data != "undefined") {
            if (isNaN(data.expire) || data.expire >= Date.now()) {
                if (_debug) _hitCount++;
                return data.value;
            } else {
                // free some space
                if (_debug) _missCount++;
                _size--;
                delete _cache[key];
            }
        } else if (_debug) {
            _missCount++;
        }
        return null;
    };

    this.size = function() {
        return _size;
    };

    this.memsize = function() {
        var size = 0,
          key;
        for (key in _cache) {
            size++;
        }
        return size;
    };

    this.debug = function(bool) {
        _debug = bool;
    };

    this.hits = function() {
        return _hitCount;
    };

    this.misses = function() {
        return _missCount;
    };

    this.keys = function() {
        return Object.keys(_cache);
    };

    this.exportJson = function() {
        var plainJsCache = {};

        // Discard the `timeout` property.
        // Note: JSON doesn't support `NaN`, so convert it to `'NaN'`.
        for (var key in _cache) {
            var record = _cache[key];
            plainJsCache[key] = {
                value: record.value,
                expire: record.expire || 'NaN',
            };
        }

        return JSON.stringify(plainJsCache);
    };

    this.importJson = function(jsonToImport, options) {
        var cacheToImport = JSON.parse(jsonToImport);
        var currTime = Date.now();

        var skipDuplicates = options && options.skipDuplicates;

        for (var key in cacheToImport) {
            if (cacheToImport.hasOwnProperty(key)) {
                if (skipDuplicates) {
                    var existingRecord = _cache[key];
                    if (existingRecord) {
                        if (_debug) {
                            console.log('Skipping duplicate imported key \'%s\'', key);
                        }
                        continue;
                    }
                }

                var record = cacheToImport[key];

                // record.expire could be `'NaN'` if no expiry was set.
                // Try to subtract from it; a string minus a number is `NaN`, which is perfectly fine here.
                var remainingTime = record.expire - currTime;

                if (remainingTime <= 0) {
                    // Delete any record that might exist with the same key, since this key is expired.
                    this.del(key);
                    continue;
                }

                // Remaining time must now be either positive or `NaN`,
                // but `put` will throw an error if we try to give it `NaN`.
                remainingTime = remainingTime > 0 ? remainingTime : undefined;

                this.put(key, record.value, remainingTime);
            }
        }

        return this.size();
    };
}

var cache = new Cache();
